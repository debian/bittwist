# Bit-Twist: Libpcap-based Ethernet packet generator

SPDX-License-Identifier: GPL-2.0-or-later

Supported systems: Windows, Linux, BSD, and macOS.

Bit-Twist is a simple yet powerful libpcap-based Ethernet packet generator and
packet editor. It is designed to complement tcpdump, which by itself has done a
great job at capturing network traffic.

> With Bit-Twist, you can now regenerate your captured traffic onto a live
> network! Packets are generated from tcpdump trace file (.pcap file).
> Bit-Twist comes with a comprehensive trace file editor to allow you to change
> the contents of a trace file.

> Bit-Twist is designed for exceptional speed, allowing you to edit **10 million
> packets in under 3 seconds** and send them over a live network at throughput
> levels that match your NIC's maximum line rate, **up to 10 Gbps**.

Bit-Twist is ideal for simulating network traffic and scenarios, testing
firewalls, IDS, IPS, routers, switches, load balancers, and other network
equipment, as well as troubleshooting various network issues.

*******************************************************************************
## FEATURES

These are just a few significant features that makes Bit-Twist unique and
stands out as one of the best Ethernet packet generator and packet editor
package made available to the open source community.

- Highly portable: Bit-Twist runs on Windows, Linux, BSD, and macOS.

- Send multiple trace files indefinitely with set interval, packets per second,
  or line rate between 1 Mbps to 10 Gbps using built-in token bucket algorithm.

- Comprehensive trace file editor to edit most fields in Ethernet, VLAN, ARP,
  IPv4, IPv6, ICMPv4, ICMPv6, TCP, and UDP headers. Templates are also included
  to generate packets with these headers without needing an existing trace file.

- Automatic header checksum correction (with option to disable).

- Send packets with custom QoS bits to test classification and queuing features
  of switches and routers.

- Send packets with uniformly distributed random numbers for IP addresses,
  port numbers, TCP sequence numbers, etc.

- Append custom payload (e.g. copy of hex stream from Wireshark) to existing
  packets after a specific header, handy for testing new protocols.

- Send packets with truncated or expanded length in bytes, facilitating
  incremental throughput testing.

- Highly scriptable: With proper manipulation, you can turn Bit-Twist into a
  versatile packet generator and packet editor tooling to meet your network
  testing requirements.

For the complete feature list, see Bit-Twist man pages:

- [bittwist.1](https://bittwist.sourceforge.io/doc/bittwist.1.html) - pcap based ethernet packet generator
- [bittwiste.1](https://bittwist.sourceforge.io/doc/bittwiste.1.html) - pcap capture file editor

*******************************************************************************
## EXAMPLES

Please visit https://bittwist.sourceforge.io/doc.html for examples on how to
use Bit-Twist.

*******************************************************************************
## INSTALLATION

Follow the instructions for your operating system below to install, run, or
build Bit-Twist on your machine.

Unless specified otherwise:

- executables (bittwist, bittwiste) are installed in /usr/local/bin
- manual pages (bittwist.1, bittwiste.1) are installed in /usr/local/share/man/man1

For more general information, please visit https://bittwist.sourceforge.io

*******************************************************************************
## FOR WINDOWS SYSTEMS

This distribution is tested to work on Microsoft Windows 11.

### Installation

- Download Npcap installer from https://npcap.com/dist/npcap-1.80.exe

- Run the Npcap installer to install Npcap on your system.
  Select "Install Npcap in WinPcap API-compatible Mode" option during the
  installation.

- Extract bittwist-windows-4.7.zip into C:\

- In Command Prompt:

```
> cd C:\bittwist-windows-4.7\bin
> .\bittwist -h   (usage for packet generator)
> .\bittwiste -h  (usage for packet editor)
> .\bittwist -d   (to view available network cards you can send packets on)
```

- If you don't see any output, it means you haven't installed Npcap yet.
  If that's the case, please install it first. Otherwise, proceed with using
  Bit-Twist from the bin directory.

- If you are new to Bit-Twist, please review the documentation at
  https://bittwist.sourceforge.io to learn how to generate and send packets
  using Bit-Twist.

- If you wish to install Bit-Twist system-wide, copy the executables from bin
  directory into C:\WINDOWS\system32:
  - bittwist.exe
  - bittwiste.exe

- Manual pages are available in doc/

### Recompilation

This distribution is compiled against Npcap 1.80 with Npcap SDK 1.13 in
MinGW-w64 environment on Linux.

If you wish to rebuild Bit-Twist from source files, you will need MinGW-w64
environment:

- Download MinGW-w64 from https://www.mingw-w64.org/downloads/

```
$ unzip bittwist-windows-4.7.zip
$ cd bittwist-windows-4.7
$ make windows
```

- Copy the generated executables from bin directory into your Windows system:
  - bittwist.exe
  - bittwiste.exe

*******************************************************************************
## FOR LINUX SYSTEMS

This distribution is tested to work on Ubuntu 24.04.1 LTS.

### Installation

```
$ tar -xzf bittwist-linux-4.7.tar.gz
$ cd bittwist-linux-4.7
```

- Precompiled x86_64 executables are included and ready for use as below:

```
$ ./bin/bittwist -h   (usage for packet generator)
$ ./bin/bittwiste -h  (usage for packet editor)
$ ./bin/bittwist -d   (to view available network cards you can send packets on)
```

- Precompiled aarch64 executables for SBCs, e.g. Raspberry Pi:

```
$ ./bin/bittwist-aarch64 -h
$ ./bin/bittwiste-aarch64 -h
```

### Recompilation

- Libpcap is required (available for download from https://www.tcpdump.org/).
  This distribution is compiled against libpcap 1.10.5. However, any existing
  libpcap on your system may work normally with Bit-Twist.

```
$ tar -xzf bittwist-linux-4.7.tar.gz
$ cd bittwist-linux-4.7
$ make
$ sudo make install
```

*******************************************************************************
## FOR MACOS SYSTEMS

This distribution is tested to work on macOS Sonoma 14.6.1

### Installation

- Libpcap is required (available for download from https://www.tcpdump.org/).
  This distribution is compiled against libpcap 1.10.5. However, any existing
  libpcap on your system may work normally with Bit-Twist.

- Xcode command line developer tools; you will be prompted to install this
  automatically on your first attempt to run make below.

```
$ tar -xzf bittwist-macos-4.7.tar.gz
$ cd bittwist-macos-4.7
$ make
$ sudo make install
```

*******************************************************************************
## FOR BSD SYSTEMS

This distribution is tested to work on FreeBSD 14.1-RELEASE.

### Installation

- Libpcap is required (available for download from https://www.tcpdump.org/).
  This distribution is compiled against libpcap 1.10.5. However, any existing
  libpcap on your system may work normally with Bit-Twist.

```
$ tar -xzf bittwist-bsd-4.7.tar.gz
$ cd bittwist-bsd-4.7
$ gmake
$ sudo gmake install
```
